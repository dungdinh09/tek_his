
import config from '../config';
var uuid = require('uuid');

// Hàm lấy tất cả bệnh nhân
const getPatients = (request, response) => {
  config.pool.query('SELECT resource from patient', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

// Hàm lấy bệnh nhân theo mã bệnh nhân
const getPatientById = (request, response) => {

  const id = request.body.id
  config.pool.query("SELECT resource->'name'->0->'use'->0 from patient WHERE id=$1", [id] , (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

// Hàm tạo mới bệnh nhân
const createPatient = (request, response) => {
  var id = uuid.v4();
  config.pool.query("INSERT INTO patient (id, txid, resource_type, status, resource) VALUES ($1, 0, 'patient', 'created', '{\"id\":\"17510601901\", \"gender\":\"male\"}')", [id],(error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json({'message': 'Tạo mới thành công', 'id': id})
  })
  
}

// Hàm cập nhật bệnh nhân
const updatePatient = (request, response) => {
  const id = request.body.id;
  const txid = 0;
  const resource_type = 'patient';

  var resource = {};
  config.pool.query("SELECT resource FROM patient WHERE id=$1 ", [id], (error, results) => {
    if (error) {
      throw error
    } 
    resource = results.rows;
    response.status(200).json({'Messages':'Lấy dữ liệu resource','Resource':resource});
  })

  // const resource = '{"id":"1751060187","name":[{"use":"officical","given":["HackerLam"],"family":"Hiếu","prefix":["Mr."]}] ,"gender":"male", "birthDate":"1999-05-10"}' 

  /*
  UPDATE patient 
  SET resource = jsonb_set(resource, '{name}'::text[], (((resource-> 'name')-(SELECT i FROM generate_series(0, jsonb_array_length(resource->'name') - 1) AS i
  WHERE (resource->'name'->i->>'use'='Không thay đổi')))::jsonb || '{"use":"thay đổi", "given":["Hiếu"],"family":"Đinh Hữu"}'::jsonb))
  WHERE id='97a0a4e5-872e-4bfe-9a76-0e6c6a64dbf8'
  */
  // test query Update trong code
  config.pool.query("UPDATE patient SET resource = jsonb_set(resource, '{name}'::text[], (((resource-> 'name')-(SELECT i FROM generate_series(0, jsonb_array_length(resource->'name') - 1) AS i WHERE (resource->'name'->i->>'use'='thay đổi')))::jsonb || '{\"use\":\"thay đổi\", \"given\":[\"Hiếu\"],\"family\":\"Đinh Hữu\"}'::jsonb)) WHERE id=$1", [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json({"Message":"Update Thành Công", "ID":id})
  })

/*
  config.pool.query("UPDATE patient SET txid=$2, resource_type=$3, resource=$4 where id =$1", [id, txid, resource_type, resource], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json({"Message":"Update Thành Công", "ID":id})
  })
*/
}

// Hàm xóa bệnh nhân
const deletePatient = (request, response) => {
  // TODO
}

module.exports = {
    getPatients,
    getPatientById,
    createPatient,
    updatePatient,
    deletePatient,
}