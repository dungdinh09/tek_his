const { v4: uuidv4 } = require('uuid');
import config from '../config';
import jsonPatient from '../resource/patient.js';

// Hàm lấy tất cả bệnh nhân
const getPatients = (request, response) => {
  config.pool.query('SELECT resource from patient', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

// Hàm lấy bệnh nhân theo mã bệnh nhân
const getPatientById = (request, response) => {

  const id = request.body.id
  config.pool.query('SELECT resource FROM patient WHERE id=$1', [id] , (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

// Hàm tạo mới bệnh nhân
const createPatient = (request, response) => {
  
  config.pool.query("INSERT INTO patient (id, txid, ts, resource_type, status, resource) VALUES (175106050558, 0, '2020-11-20 12:00:00.236978+07', 'patient', 'created', '{\"id\":\"175106050558\", \"gender\":\"male\"}')", (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json({"Message":"Insert Thành Công"})
  })
}

// Hàm cập nhật bệnh nhân
const updatePatient = (request, response) => {

  const id = request.body.id
  config.pool.query("UPDATE patient SET txid='4', resource_type='patient', resource='{\"id\":\"1751060187\",\"name\":[{\"use\":\"officical\",\"given\":[\"HackerLam\"],\"family\":\"Hieu\",\"prefix\":[\"Mr.\"]}], \"gender\":\"female\", \"birthDate\":\"1999-05-10\"}' where id ='1751060187'", (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json({"Message":"Update Thành Công"})
  })
}

// Hàm xóa bệnh nhân
const deletePatient = (request, response) => {
  // TODO
}

module.exports = {
    getPatients,
    getPatientById,
    createPatient,
    updatePatient,
    deletePatient,
}