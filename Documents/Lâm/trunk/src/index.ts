
// tslint:disable-next-line:no-var-requires
const services = require('./services')
// tslint:disable-next-line:no-var-requires
const express = require("express");
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.get('/api/getPatients', services.getPatients)

app.post('/api/getPatientById', services.getPatientById)

app.post('/api/createPatient', services.createPatient)

// Bat dau van hanh server
async function startServer() {

  // await require('./loaders').default({ expressApp: app });

  app.listen(3000, err => {
    if (err) {
      // tslint:disable-next-line:no-console
      console.log(err);
      return;

    }

    // tslint:disable-next-line:no-console
    console.log(`Your server is ready !`);
  });
}

// Run the async function to start our server
startServer();