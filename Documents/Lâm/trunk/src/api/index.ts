import { Router } from 'express';
import patient from './routes/patient';

// guaranteed to get dependencies
export default () => {
	const app = Router();
	patient(app);

	return app
}