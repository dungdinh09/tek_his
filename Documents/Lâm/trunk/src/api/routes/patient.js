import { Router, Request, Response } from 'express';
const patientService = require('./services/patient');
const route = Router();

export default (app) => {
    app.use('/api/getPatients', route);
  

    route.get('/api/getPatients', patientService.getPatients);


  };