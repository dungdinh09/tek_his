
import config from '../config';
var uuid = require('uuid');

// Hàm lấy tất cả bệnh nhân
const getPatients = (request, response) => {
  config.pool.query('SELECT resource from patient', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

// Hàm lấy bệnh nhân theo mã bệnh nhân
const getPatientById = (request, response) => {

  const id = request.body.id
  config.pool.query("SELECT resource->'name'->0->'use'->0 from patient WHERE id=$1", [id] , (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

// Hàm tạo mới bệnh nhân
const createPatient = (request, response) => {
  var id = uuid.v4();
  config.pool.query("INSERT INTO patient (id, txid, resource_type, status, resource) VALUES ($1, 0, 'patient', 'created', '{\"id\":\"17510601901\", \"gender\":\"male\"}')", [id],(error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json({'message': 'Tạo mới thành công', 'id': id})
  })
  
}

// Hàm cập nhật bệnh nhân
const updatePatient = (request, response) => {
  // TODO
}

// Hàm xóa bệnh nhân
const deletePatient = (request, response) => {
  // TODO
}

module.exports = {
    getPatients,
    getPatientById,
    createPatient,
    updatePatient,
    deletePatient,
}